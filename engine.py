"""
The engine module can be used directly with the Template class, this accepts
a string representing the template you want to use. Calling template.render(context)
will render the template with the given context (a dictionary mapping names
to values for use in the template).

To use the engine module with files containing templates and allowing templates
to be extended you must extend the TEMPLATE_DIRS module-level list to indicate
what directories contain your templates. After this you can use the module-level
function 'render' with a template filename (relative to any directory in 
TEMPLATE_DIRS) and a context in order to render that template.

Use the templating engine within your given text using
the following string templating syntax:

## Grammar ##

NAME       ::= [A-Za-z0-9_]+
PATH       ::= [/A-Za-z0-9_.]+
EXPRESSION ::= NAME # TODO: extend to include function/member calls etc.
CONDITION  ::= EXPRESSION
STATEMENT  ::= if CONDITION | else | endif | for NAME in EXPRESSION | endfor | extends PATH
SUB        ::= {{ EXPRESSION }}
TAG        ::= {% STATEMENT %}

## String substitution ##

{{ EXPRESSION }}

e.g. {{ first_name }} # reference value with key 'first_name' in context and substitute

## If Statement ##

{% if CONDITION %}
some text here
{% else %}
some text if CONDITION evaluates to false
{% endif %}

## For Loop ##

{% for item in EXPRESSION %}

A python object you want processed for each item in EXPRESSION
EXPRESSION should evaluate to a Python iterable

Can also use {{ item }} to substitute the item variable or any
EXPRESSION involving 'item' that evaluates to a substitutable value

{% endfor %}

## template inheritance ##

{% extends /path/to/other/template.tmpl %}
"""


import copy
import os
import re

from collections import defaultdict

TEMPLATE_DIRS = []


class TemplateError(Exception):
    pass


class LexerError(TemplateError):
    pass


class ParserError(TemplateError):
    pass


class Lexer(object):

    class Scanner(object):
        def __init__(self, string):
            self.string = string

            self.lineno = 1

            self._index = -1

        def is_eof(self):
            return self._index + 1 >= len(self.string)

        def peek(self, i=1):
            try:
                return self.string[self._index + i]
            except IndexError:
                return ""

        def peek_back(self, i=1):
            index = self.index - i
            if index >= 0:
                return self.string[self._index]
            else:
                return ""

        def next(self):
            self._index += 1
            try:
                char = self.string[self._index]

                if char == "\n":
                    self.lineno += 1

                return char
            except IndexError:
                raise StopIteration

        def __iter__(self):
            return self
    
    def __init__(self, string):
        self.string = string

    def tokenize(self):
        return [token for token in self.next_token()]

    def next_token(self):
        scanner = self.Scanner(self.string)

        value = ""
        for char in scanner:
            if char == "{":
                # tag opens with "{%"
                if scanner.peek() == "%":
                    if value:
                        yield { "type": "text", "value": value, "lineno": scanner.lineno }
                        value = ""

                    yield { "type": "tag", "value": self._match_tag(scanner), "lineno": scanner.lineno }
                # sub opens with "{{"
                elif scanner.peek() == "{":
                    if value:
                        yield { "type": "text", "value": value, "lineno": scanner.lineno }
                        value = ""

                    yield { "type": "sub", "value": self._match_sub(scanner), "lineno": scanner.lineno }
                else:
                    value += char
            else:
                value += char

        if value:
            yield { "type": "text", "value": value, "lineno": scanner.lineno }

        raise StopIteration

    def _match_tag(self, scanner):
        scanner.next() # consume "%"

        contents = ""
        for char in scanner:
            if char == "%" and scanner.peek() == "}":
                break
            else:
                contents += char

        scanner.next() # consume "}"

        # if the tag is on a line of its own, remove its eol char
        # if scanner.peek() == "\n" and scanner.peek(2) == "\n":
        #     scanner.next() # consume end of line

        return contents.strip()

    def _match_sub(self, scanner):
        scanner.next() # consume "{"

        contents = ""
        for char in scanner:
            if char == "}" and scanner.peek() == "}":
                break
            else:
                contents += char

        scanner.next() # consume "}"

        # if the sub is on a line of its own, remove its eol char
        # if scanner.peek() == "\n" and scanner.peek(2) == "\n":
        #     scanner.next() # consume end of line

        return contents.strip()


class Parser(object):

    TAG_INFO_REGEX = re.compile(r"(?P<type>\w+)\s*(?P<args>([^\s]+\s*)*)")
    SUB_EXPR_REGEX = re.compile(r"\w+")

    def __init__(self, tokens):
        self.tokens = tokens

    def parse(self):
        root_node = ParseNode("root", -1)

        node_stack = [root_node]

        for token in self.tokens:
            stack_top = node_stack[-1]

            if token["type"] == "tag":
                # get node info from regex
                m = self.TAG_INFO_REGEX.match(token["value"])
                node_type = m.group("type")
                node_args = m.group("args").split()

                node = self._create_tag_node(node_type, node_stack, token["lineno"], *node_args)
            elif token["type"] == "sub":
                m = self.SUB_EXPR_REGEX.match(token["value"])
                expr = m.group(0)

                node = NodeSub(expr, token["lineno"])
            elif token["type"] == "text":
                node = NodeText(token["value"], token["lineno"])

            # 'else' is a special case node
            if node.node_type in ["else"]:
                continue

            # this is an 'end' node
            if node.node_type != "extends":
                m = re.match(r"end(\w+)", node.node_type)
                if m:
                    end_type = m.group(1)

                    if len(node_stack) <= 1:
                        raise ParserError("Line %d: No opening tag for %s." % (node.lineno, node.node_type))
                    elif end_type == "if" and stack_top.node_type not in ["if", "else"]:
                        raise ParserError("Line %d: End tag type 'endif' does not have an associated 'if' tag." % (node.lineno))
                    elif end_type != "if" and stack_top.node_type != end_type:
                        raise ParserError("Line %d: End tag type '%s' does not match previously opened tag '%s'." % (node.lineno, node.node_type, stack_top.node_type))

                    # nothing more needs to be done with an 'end' tag but to remove
                    # the stack top node that should relate to it
                    node_stack.pop()
                    continue

            try:
                # add node as child to node at top of stack
                stack_top.addChild(node)
            except IndexError:
                raise ParserError("Line %d: No root ParseNode found for token %s." % (token["lineno"], token))

        if len(node_stack) > 1:
            raise ParserError("Line %d: Unclosed tag: %s." % (node_stack[-1].lineno, node_stack[-1].node_type))

        return root_node

    def _create_tag_node(self, node_type, node_stack, lineno, *args):
        stack_top = node_stack[-1]

        if node_type == "extends":
            extend_node = NodeExtends(*args, lineno=lineno)
            return extend_node
        elif node_type == "if":
            if_node = NodeIf(*args, lineno=lineno)
            node_stack.append(if_node)
            return if_node
        elif node_type == "else":
            if stack_top.node_type != "if":
                raise ParserError("Line %d: Else tag must be associated with If. Found '%s' instead." % (lineno, stack_top.node_type))

            return stack_top.addElse(lineno)
        elif node_type == "for":
            for_node = NodeFor(*args, lineno=lineno)
            node_stack.append(for_node)
            return for_node
        elif node_type == "block":
            block_node = NodeBlock(*args, lineno=lineno)
            node_stack.append(block_node)
            return block_node
        else:
            return ParseNode(node_type, lineno)

class OutputGenerator(object):

    def run(self, ast, context):
        for node in ast:
            self._write_node(node, context)

    def _write_node(self, node, context):
        func = getattr(self, "_write_%s" % node.node_type)
        func(node, context)

    def _write_sub(self, node, context):
        pass

    def _write_text(self, node, context):
        pass

    def _write_if(self, node, context):
        pass

    def _write_else(self, node, context):
        pass

    def _write_for(self, node, context):
        pass

    def _write_block(self, node, context):
        pass


class ParseNode(object):
    
    def __init__(self, node_type, lineno):
        self.node_type = node_type
        self.lineno = lineno

        self.parent = None

        self._children = []

    def addChild(self, node):
        node.parent = self
        self._children.append(node)

    def remove_child(self, node):
        self._children.remove(node)
        node.parent = None

    def insert_child(self, index, node):
        self._children.insert(index, node)

    def child_index(self, node):
        return self._children.index(node)

    def index(self):
        if self.parent:
            return self.parent.child_index(self)
        return 0

    def evaluate(self, context):
        text = ""
        for node in self._children:
            text += node.evaluate(context)
        return text

    def find(self, pred):
        """
        Find nodes which cause the given predicate
        to return True.

        :Parameters:
            pred: callable
                a callable that should take a node and return True or False if the node should be included
        """

        found_nodes = []

        for node in self._children:
            found_nodes.extend(node.find(pred))

            if pred(node):
                found_nodes.append(node)

        return found_nodes

    def pprint(self, indent=0):
        spacing = " " * indent
        print "%s%s" % (spacing, self)

        for node in self._children:
            node.pprint(indent + 4)

    def __str__(self):
        return unicode(self)

    def __unicode__(self):
        s = u"[<%s>: %s]" % (type(self).__name__, self.node_type)
        return s

    def __repr__(self):
        return unicode(self)


class NodeText(ParseNode):

    def __init__(self, text, lineno):
        super(NodeText, self).__init__("text", lineno)

        self.text = text

    def evaluate(self, context):
        return self.text

    def __str__(self):
        return unicode(self)

    def __unicode__(self):
        return u"[<NodeText>: %s]" % repr(self.text)


class NodeExtends(ParseNode):

    def __init__(self, path, lineno):
        super(NodeExtends, self).__init__("extends", lineno)

        self.path = path

    def __str__(self):
        return unicode(self)

    def __unicode__(self):
        return u"[<NodeExtends: %s]" % repr(self.path)


class NodeSub(ParseNode):

    def __init__(self, expr, lineno):
        super(NodeSub, self).__init__("sub", lineno)

        self.expr = expr

    def evaluate(self, context):
        return unicode(context[self.expr])

    def __str__(self):
        return unicode(self)

    def __unicode__(self):
        return u"[<NodeSub>: %s]" % self.expr


class NodeIf(ParseNode):

    def __init__(self, condition, lineno):
        super(NodeIf, self).__init__("if", lineno)

        self._condition = condition

        self._else = None

    def addChild(self, node):
        if self._else:
            self._else.addChild(node)
        else:
            super(NodeIf, self).addChild(node)

    def addElse(self, lineno):
        self._else = NodeElse(lineno)
        return self._else

    def evaluate(self, context):
        if self._condition not in context:
            return self._else.evaluate(context)
        elif context[self._condition]:
            return super(NodeIf, self).evaluate(context)
        elif self._else:
            return self._else.evaluate(context)

        return ""

    def pprint(self, indent=0):
        spacing = " " * indent
        print "%s%s" % (spacing, self)

        for node in self._children:
            node.pprint(indent + 4)

        if self._else:
            self._else.pprint(indent)


class NodeElse(ParseNode):

    def __init__(self, lineno):
        super(NodeElse, self).__init__("else", lineno)

    def evaluate(self, context):
        return super(NodeElse, self).evaluate(context)


class NodeFor(ParseNode):

    def __init__(self, varname, itername, lineno):
        super(NodeFor, self).__init__("for", lineno)

        self._varname = varname
        self._itername = itername

    def evaluate(self, context):
        text = ""

        # execute ParseNode.evaluate() for each iterator pass
        for value in context[self._itername]:
            new_context = copy.deepcopy(context)
            new_context[self._varname] = value
            text += super(NodeFor, self).evaluate(new_context)

        return text


class NodeBlock(ParseNode):

    def __init__(self, name, lineno):
        super(NodeBlock, self).__init__("block", lineno)

        self.name = name


class Template(object):

    def __init__(self, filename):
        self.filename = filename

        self._full_path = None

        # try and find the template denoted by 'filename'
        # relative to any of the template directories
        for template_dir in TEMPLATE_DIRS:
            full_path = os.path.join(template_dir, filename)

            try:
                self._template = open(full_path).read()
                self._full_path = full_path
            except IOError:
                pass
            else:
                break

        if not self._full_path:
            raise TemplateError("Could not find template file '%s'. Perhaps you haven't added its containing directory to TEMPLATE_DIRS?" % self.filename)

        # do lexing and parsing now because it only needs to be done once
        tokens = Lexer(self._template).tokenize()
        self._root_node = Parser(tokens).parse()

        self._combine_inherited(self._root_node)

    def render(self, context=None):
        context = context or {}

        return self._root_node.evaluate(context)

    def _combine_inherited(self, root):
        """
        Recurse up the inheritance hierarchy for all templates and
        combine their parse trees to form the full template.

        The base parse tree is taken as the top template in the 
        hierarchy and any blocks that are defined in inheriting
        templates will override those further up in the hierarchy.
        """

        extend_nodes = root.find(lambda n: n.node_type == "extends")

        if not extend_nodes:
            return
        elif len(extend_nodes) > 1:
            raise TemplateError("Cannot have more than one 'extends' tag, only single inheritance is supported.")

        node = extend_nodes[0]
        template = Template(node.path)

        parent_root = template._root_node

        blocks = root.find(lambda n: n.node_type == "block")
        parent_blocks = parent_root.find(lambda n: n.node_type == "block")
        for block in blocks:
            matches = filter(lambda b: b.name == block.name, parent_blocks)
            if matches:
                # replace current block with block that overrides this one
                parent_block = matches[0]
                index = parent_block.index()
                parent_block.parent.insert_child(index, block)
                parent_block.parent.remove_child(parent_block)

        self._root_node = parent_root


def render(filename, context=None):
    template = Template(filename)

    return template.render(context)

if __name__ == "__main__":
    TEMPLATE_DIRS.append("~/templates")

    # from pprint import pprint
    # lexer = Lexer("import os\nif 0 == 0:\n    print {{ variable }}{% if flag %}, 'World'{% else %}, 'London'{% endif %}")
    # tokens = lexer.tokenize()

    # parser = Parser(tokens)
    # node = parser.parse()

    # print node.evaluate({ "variable": repr("Hello"), "flag": False })
